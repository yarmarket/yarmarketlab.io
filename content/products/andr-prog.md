{
    "title":"Android для программистов. Создаем приложения.",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["Android", "book", "Java"],
    "categories": ["Android", "Java"],
    "images": ["img/andr-prog/andr-prog1.jpg", "img/andr-prog/andr-prog2.jpg"],
    "thumbnailImage": "img/andr-prog/thumbnail.jpg",
    "actualPrice": "80.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "80.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: Android для программистов, Создаем приложения, 
П.Дейтел, Х.Дейтел, Э.Дейтел, М.Моргано.