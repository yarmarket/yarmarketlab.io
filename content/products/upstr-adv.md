{
    "title":"Upstream, Advanced C1, Students book, Virginia Evans, Lynda Edwards, Jenny Dooley",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["English", "book"],
    "categories": ["English"],
    "images": ["img/upstr-adv/upstr-adv1.jpg", "img/upstr-adv/upstr-adv2.jpg"],
    "thumbnailImage": "img/upstr-adv/thumbnail.jpg",
    "actualPrice": "1,100.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"      
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "1,100.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "Upstream, Advanced C1, Students book, Virginia Evans, Lynda Edwards, Jenny Dooley"