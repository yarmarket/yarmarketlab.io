{
    "title":"English Idioms in Use, Intermediate",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["English", "book"],
    "categories": ["English"],
    "images": ["img/idiom/idiom1.jpg", "img/idiom/idiom2.jpg"],
    "thumbnailImage": "img/idiom/thumbnail.jpg",
    "actualPrice": "450.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"      
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "450.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: English Idioms in Use, Intermediate