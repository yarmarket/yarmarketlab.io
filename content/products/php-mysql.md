{
    "title":"PHP Создание интернет-магазина, Ларри Ульман",
    "date": "2020-05-15T22:35:06+05:30",
    "weight": 1,
    "tags": ["php", "book"],
    "categories": ["php"],
    "images": ["img/php-mysql/php-mysql1.jpg", "img/php-mysql/php-mysql2.jpg"],
    "thumbnailImage": "img/php-mysql/thumbnail.jpg",
    "actualPrice": "850.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний" 
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "850.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "PHP Создание интернет-магазина, Ларри Ульман"