{
    "title":"Market Leader, Work Book, Upper Intermediate, 3rd Edition",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["English", "book"],
    "categories": ["English"],
    "images": ["img/ml-wb-upper/ml-wb-upper1.jpg", "img/ml-wb-upper/ml-wb-upper2.jpg"],
    "thumbnailImage": "img/ml-wb-upper/thumbnail.jpg",
    "actualPrice": "400.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"      
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "400.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: Market Leader, Work Book, Upper Intermediate, 3rd Edition