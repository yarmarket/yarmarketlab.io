{
    "title":"Создание приложений для Android за 24 часа",
    "date": "2020-05-12T22:35:06+05:30",
    "weight": 1,
    "tags": ["Android", "book", "Java"],
    "categories": ["Android", "Java"],
    "images": ["img/andr/andr1.jpg", "img/andr/andr2.jpg"],
    "thumbnailImage": "img/andr/thumbnail.jpg",
    "actualPrice": "350.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"       
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "350.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: Создание приложений для Android за 24 часа, Кармен Делессио, Лорен Дарси, Шейн Кондер.