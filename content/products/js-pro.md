{
    "title":"JavaScript Для профессиональных веб-разработчиков, Нииколас Закас",
    "date": "2020-05-15T22:35:06+05:30",
    "weight": 1,
    "tags": ["javascript", "book"],
    "categories": ["JavaScript"],
    "images": ["img/js-pro/js-pro1.jpg", "img/js-pro/js-pro2.jpg"],
    "thumbnailImage": "img/js-pro/thumbnail.jpg",
    "actualPrice": "650.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний" 
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "650.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "JavaScript Для профессиональных веб-разработчиков, Нииколас Закас"