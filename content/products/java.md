{
    "title":"Программирование на Java, Исчерпывающее руководство для профессионалов, Патрик Нимейер, Дэниэл Леук",
    "date": "2020-05-15T22:35:06+05:30",
    "weight": 1,
    "tags": ["php", "book"],
    "categories": ["java"],
    "images": ["img/java/java1.jpg", "img/java/java2.jpg"],
    "thumbnailImage": "img/java/thumbnail.jpg",
    "actualPrice": "1,100.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний" 
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "1,100.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "Программирование на Java, Исчерпывающее руководство для профессионалов, Патрик Нимейер, Дэниэл Леук"