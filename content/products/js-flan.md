{
    "title":"JavaScript Подробное руководство, Дэвид Флэнаган",
    "date": "2020-05-15T22:35:06+05:30",
    "weight": 1,
    "tags": ["javascript", "book"],
    "categories": ["JavaScript"],
    "images": ["img/js-flan/js1.jpg", "img/js-flan/js2.jpg"],
    "thumbnailImage": "img/js-flan/thumbnail.jpg",
    "actualPrice": "500.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний" 
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "actualPrice": "800.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "Javascript Подробное руководство, Дэвид Флэнаган, 6-е издание"
 
Гарна книга для тих кто бажає розібратися у всих деталях Javascript;