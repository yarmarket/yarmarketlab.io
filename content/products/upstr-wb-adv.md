{
    "title":"Upstream, Advanced C1, Workbook, Virginia Evans, Lynda Edwards",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["English", "book"],
    "categories": ["English"],
    "images": ["img/upstr-wb-adv/upstr-wb-adv1.jpg", "img/upstr-wb-adv/upstr-wb-adv2.jpg"],
    "thumbnailImage": "img/upstr-wb-adv/thumbnail.jpg",
    "actualPrice": "1,000.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"      
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "1,000.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "Upstream, Advanced C1, Workbook, Virginia Evans, Lynda Edwards"