{
    "title":"Market Leader, Students Book, Upper Intermediate, 3rd Edition",
    "date": "2020-05-12T22:35:06+05:30",
    "tags": ["English", "book"],
    "categories": ["English"],
    "images": ["img/ml-sb-upper/ml-sb-upper1.jpg", "img/ml-sb-upper/ml-sb-upper2.jpg"],
    "thumbnailImage": "img/ml-sb-upper/thumbnail.jpg",
    "actualPrice": "480.00",
    "currency": "грн",    
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний"      
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "480.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: Market Leader, Students Book, Upper Intermediate, 3rd Edition