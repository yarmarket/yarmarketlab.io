{
    "title":"Регулярные выражения, Джеффри Фрида",
    "date": "2020-05-15T22:35:06+05:30",
    "weight": 1,
    "tags": ["regex", "book"],
    "categories": ["misc"],
    "images": ["img/regex/regex1.jpg", "img/regex/regex2.jpg"],
    "thumbnailImage": "img/regex/thumbnail.jpg",
    "actualPrice": "550.00",
    "currency": "грн",
    "comparePrice": null,
    "inStock": true,
    "comment": "Телефонуйте +38(063) 602-90-07",
    "options": {
            "Стан" : "Відмінний" 
    },
    "variants": [
        {
            "optionCombination" : ["Відмінний"],
            "comparePrice": null,
            "actualPrice": "550.00",
            "currency": "грн",
            "inStock": true,
            "auction": true
        }  
    ]
}

Книга: "Регулярные выражения, Джеффри Фрида"