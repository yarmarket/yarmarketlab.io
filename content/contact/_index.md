---
title: "Contact"
description : "this is a meta description"

office:
  title : "Контакти"
  mobile : "+38(063) 602-90-07"

# opennig hour
opennig_hour:
  title : "Ми працюємо:"
  day_time:
    - "Пн: 9:00 – 19:00"
    - "Вт: 9:00 – 19:00"
    - "Ср: 9:00 – 19:00"
    - "Чт: 9:00 – 19:00"
    - "Пт: 9:00 – 19:00"
    - "Сб: 9:00 – 19:00"
    - "Нд: 9:00 – 19:00"
    
draft: false
---

